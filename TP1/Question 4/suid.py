import os
# get the group id
# of the current process
# using os.getuid() method

euid = os.getuid()
egid = os.getgid()

#print the goup id #of the current process

print("Effective user id of the current process : ", euid)
print("Effective group id of the current process : ", egid)


