#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "check_pass.h"

int rmg(const char *filename){
    char password[20];
    char *encryptedpassword;
    uid_t uid;
    int is_not_deleted;
    int a;
    uid = getuid();

    printf("Please enter a password :\n");
    scanf("%s",password);


    encryptedpassword = crypt(password, "sa");
    a=check_pass("/home/admin/passwd",password,uid);
    if (check_pass("/home/admin/passwd",password,uid)){
      is_not_deleted = remove(filename);
      if (!is_not_deleted){
        printf("%s file deleted\n", filename);
      }else{
        printf("%s We can't delete file\n", filename);
      }
    }else{
      printf("wrong password\n");
    }


    return 0;
}


int main(int argc, char const *argv[]){
    struct stat s;
    gid_t gid;
    if(argc != 2)
    {
       printf("please enter the file that must be deleted.\n");
    }


    stat(argv[1], &s);
    gid = getgid();

    if (gid == s.st_gid)
    {
        rmg(argv[1]);
    }
    else
    {
        printf("You don't have access.\n");
        return EXIT_FAILURE;
    }


    return EXIT_SUCCESS;
}
