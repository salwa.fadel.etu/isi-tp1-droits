#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pwd.h>


int main(int argc, char const *argv[])
{
    FILE *fd, *fd_new;
    char *line = NULL, *line_new = NULL,*pwd, *uid;
    char delim[] = ":";
    int found = 1;
    size_t len = 0;
    ssize_t size;
    char new_password[20], actual_pwd[20], *toCrackCiph;

    uid_t user_id = getuid();

    if ((fd = fopen("/home/admin/passwd", "r")) != NULL)
    {
        while (found!=0 && (size = getline(&line, &len, fd)) != -1)
        {
            uid = strtok(line, delim);
            pwd = strtok(NULL, delim);
            if (atoi(uid) == user_id)
                found = 0;
        }

    }
    fclose(fd);
    if (found == 0)
    {
        printf("actual password :\n");
        scanf("%s", actual_pwd);

        if (strcmp(pwd, crypt(actual_pwd, "sa")))
        {
            fprintf(stderr, "incorrect password !\n");
            return EXIT_FAILURE;
        }
        printf("new password: \n");
    }
    else
    {
        printf("enter the password: \n");
    }

    scanf("%s", new_password);

    if ((fd = fopen("/home/admin/passwd", "r")) != NULL && (fd_new = fopen("/home/admin/tmp", "w+")) != NULL)
    {

        while (getline(&line_new, &len, fd) != -1)
        {
            uid = strtok(line_new, delim);
            pwd = strtok(NULL, delim);
            if (atoi(uid) != user_id)
                fprintf(fd_new, "%s:%s:\n", uid,pwd);

        }
        fprintf(fd_new, "%d:%s:\n", user_id, crypt(new_password, "sa"));


    }
    fclose(fd_new);
    fclose(fd);
    rename("/home/admin/tmp", "/home/admin/passwd");


    return EXIT_SUCCESS;
}
